#!/usr/bin/env python3

import gitlab
import argparse
import requests
import time
import yaml

def protect_branch(gl, project, branch_name, protect_settings, token):
    '''
        Copy complex branch protection settings and apply them on a new branch.
        Needs to be one request, later additions are not possible,
        as the API responds that the protected branch already exists
    '''

    headers = {'PRIVATE-TOKEN': token}
    #assembling the string is pretty crude but the API has an unusual array syntax 
    #and assembling the dictionary with array keys correctly was not more elegant, just tedious
    call_payload = []
    for merge_level in protect_settings["merge_access_levels"]:
        if not merge_level["user_id"] and not merge_level["group_id"]:
            #this is set via merge_access_level
            call_payload.append("merge_access_level=" + str(merge_level["access_level"]))
        else:
            #this is set via allowed_to_merge array
            if merge_level["user_id"]:
                call_payload.append("allowed_to_merge%5B%5D%5Buser_id%5D=" + str(merge_level["user_id"]))
            if merge_level["group_id"]:
                call_payload.append("allowed_to_merge%5B%5D%5Buser_id%5D=" + str(merge_level["group_id"]))

    for push_level in protect_settings["push_access_levels"]:
        if not push_level["user_id"] and not push_level["group_id"]:
            #this is set via push_access_level
            call_payload.append("push_access_level=" + str(push_level["access_level"]))
        else:
            #this is set via allowed_to_push array
            if push_level["user_id"]:
                call_payload.append("allowed_to_push%5B%5D%5Buser_id%5D=" + str(push_level["user_id"]))
            if push_level["group_id"]:
                call_payload.append("allowed_to_push%5B%5D%5Buser_id%5D=" + str(push_level["group_id"]))

    for unprotect_level in protect_settings["unprotect_access_levels"]:
        if not unprotect_level["user_id"] and not unprotect_level["group_id"]:
            #this is set via unprotect_access_level
            call_payload.append("unprotect_access_level=" + str(unprotect_level["access_level"]))
        else:
            #this is set via allowed_to_unprotect array
            if unprotect_level["user_id"]:
                call_payload.append("allowed_to_unprotect%5B%5D%5Buser_id%5D=" + str(unprotect_level["user_id"]))
            if unprotect_level["group_id"]:
                call_payload.append("allowed_to_unprotect%5B%5D%5Buser_id%5D=" + str(unprotect_level["group_id"]))

    if protect_settings["code_owner_approval_required"]:
        call_payload.append("code_owner_approval_required=true")
    payload_string = "&".join(call_payload)
    #print("%s/projects/%s/protected_branches?name=%s&%s" % (gl.api_url, project.id, new_branch_name, payload_string))
    try:
        protect_branch = requests.post("%s/projects/%s/protected_branches?name=%s&%s" % (gl.api_url, project.id, new_branch_name, payload_string), headers=headers)
        protect_branch.raise_for_status()
    except Exception as e:
        print("Could not protect branch: %s" % protect_branch.json()["message"])

def rename_branches(gl, projects, branch_to_rename, new_branch_name, token, radical):
    changed_projects = []
    headers = {'PRIVATE-TOKEN': token}

    for project in projects:
        project_object = gl.projects.get(project)
        print("Renaming %s branch into %s in project %s" % (branch_to_rename, new_branch_name, project_object.attributes["path_with_namespace"]))
        old_branch = project_object.branches.get(branch_to_rename)
        developers_can_push = old_branch.attributes["developers_can_push"]
        developers_can_merge = old_branch.attributes["developers_can_merge"]
        protected = old_branch.attributes["protected"]
        default = old_branch.attributes["default"]

        try:
            new_branch = project_object.branches.create({'branch': new_branch_name,
                                  'ref': branch_to_rename})
        except gitlab.exceptions.GitlabCreateError:
            print("Branch already exists: %s" % new_branch_name)
            print("Unprotecting %s to ensure protect settings come over." % new_branch_name)
            project_object.branches.get(new_branch_name).unprotect()

        if protected:
            #python-gitlab does not support codeowners so it's requests from here
            protected_settings = requests.get("%s/projects/%s/protected_branches/%s" % (gl.api_url, project_object.id, branch_to_rename), headers=headers)
            print("Protecting %s." % new_branch_name)
            protect_branch(gl, project_object, new_branch_name, protected_settings.json(), token)
            if radical:
                print("Unprotecting %s." % branch_to_rename)
                old_branch.unprotect()
        if default:
            project_object.default_branch = new_branch_name
            project_object.save()

        if radical:
            print("Deleting %s." % branch_to_rename)
            old_branch.delete()

def get_group_projects(gl, groups):
    projects = []
    for topgroup in groups:
        group = gl.groups.get(topgroup)
        group_projects = group.projects.list(as_list=False, include_subgroups=True)
        for group_project in group_projects:
            projects.append(group_project.attributes["id"])
    return projects

parser = argparse.ArgumentParser(description='Create report for GitLab issues')
parser.add_argument('token', help='API token able to read the requested projects')
parser.add_argument('configfile', help='CSV file that defines requested projects')
parser.add_argument('-r','--radical', help='Radical mode. Actually delete the branch you rename. Without this flag, this script only copies branches.', action="store_true")
args = parser.parse_args()

gitlaburl = "https://gitlab.com/"
gl = gitlab.Gitlab(gitlaburl, private_token=args.token)

configfile = args.configfile
groups = []
projects = []
branch_to_rename = ""
new_branch_name = ""

with open(configfile, "r") as c:
    config = yaml.load(c, Loader=yaml.FullLoader)
    gitlaburl = config["gitlab_url"] if config["gitlab_url"].endswith("/") else config["gitlab_url"] + "/"
    gl = gitlab.Gitlab(gitlaburl, private_token=args.token)
    if "groups" in config:
        groups = config["groups"]
        projects = get_group_projects(gl, groups)
    elif "projects" in config:
        projects = config["projects"]
    elif "instance" in config:
        if config["instance"]:
            group_objects = gl.groups.list(as_list=False)
            for group in group_objects:
                groups.append(group.id)
            projects = get_group_projects(gl, groups)
    else:
        print("Error: No group or project configured. Stopping.")
        exit(1)
    branch_to_rename = config["old_branch_name"]
    new_branch_name = config["new_branch_name"]
    if not branch_to_rename or not new_branch_name:
        print("Error: Please specify old_branch_name and new_branch_name")
        exit(1)

rename_branches(gl, projects, branch_to_rename, new_branch_name, args.token, args.radical)
