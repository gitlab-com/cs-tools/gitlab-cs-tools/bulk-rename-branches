# Bulk rename branches

Rename all branches for all groups, a specific group or specific projects:

* For all specified projects:
  * Create a new branch from the old branch
  * Protect the new branch like the old branch
  * Make the new branch default
  * Optionally ("radical mode"): Unprotect and delete the old branch

## Usage

`python3 rename_branches.py $GIT_TOKEN config.yml`

Optional parameter: `-r`, `--radical`: Radical mode, unprotect and delete the old branch

## Configuration

- edit config.yml
  - specify GitLab URL and groups or projects using their ID.
  - If groups are used, all projects in the group are retrieved instead of individual projects.
  - If projects are used, they have to be specified with their id or full namespace
  - If "instance: true" is used, will run on all groups on the instance you have access to
  - specify `old_branch_name` and `new_branch_name`

## DISCLAIMER

This script performs a potentially **hazardous** operation, especially in radical mode. Renaming branches has consequences outside of the branch naming, like CI configurations, local dev configurations and the like. You may **break** all kinds of stuff. You probably **don't** want radical mode but delete master manually.

This script is provided **as is**. I take no responsibility for any consequences of your usage of this script. I offer no further support.